package info.hccis.cis2250.bjmaclean.canesmacleanbj.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;


import java.util.List;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.R;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Business;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentBusinessDetails;


/**
 * Created by kgoddard on 2/2/2018.
 */

public class BusinessAdapter extends RecyclerView.Adapter<BusinessAdapter.MyViewHolder> {
    private List<Business> businesses;
    private Context context;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.business_row, parent, false);
        Log.d("KGADAPTER", "onCreateViewHolder");
        return new MyViewHolder(itemView);
    }

    public BusinessAdapter(Context context, List<Business> businessList) {
        Log.d("KGADAPTER bjtest", "Instantiating adapter");
        this.context = context;
        this.businesses = businessList;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Business business = businesses.get(position);

        holder.businessName.setText(business.getName());
        holder.businessUsername.setText(business.getUsername());
    }

    @Override
    public int getItemCount() {
        return businesses.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView businessName, businessUsername;
        //public ImageButton expand;

        public MyViewHolder(View view) {
            super(view);
            Log.d("KGADAPTER", "Instantiating MyViewHolder");
            businessName = view.findViewById(R.id.textViewBusinessName);
            businessUsername = view.findViewById(R.id.textViewBusinessUsername);
          //  expand = view.findViewById(R.id.imageButtonExpandBooking);

            //Once we click this row we will want to show more details about this camper.
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("bjtest", "clicked row in list of businesses..."+getAdapterPosition());
                    Log.d("bjtest", "Camper details:..."+ businesses.get(getAdapterPosition()).toString());

                    //Todo  swap to details fragment...
                    //used this resource for the following syntax:  http://code.i-harness.com/en/q/1ba462f
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();

                    /* Note that I am passing the camper object based on the position that was clicked
                    in the list.  I had to modify the newInstance method to accept a camper object.  then I use
                    the code to set a serializable object in the bundle which is passed to the onStart of
                    the fragment.  This allows the camper object to get from here to the details fragment.
                     */
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment,
                            FragmentBusinessDetails.newInstance(businesses.get(getAdapterPosition()))).addToBackStack(null).commit();
                }
            });
        }
        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "clicked", Toast.LENGTH_LONG).show();
        }
    }

}
