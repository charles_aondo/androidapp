package info.hccis.cis2250.bjmaclean.canesmacleanbj.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.R;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Camper;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentHome;

/**
 * Created by kgoddard on 2/2/2018.
 */

public class CamperAdapter extends RecyclerView.Adapter<CamperAdapter.MyViewHolder> {
    private List<Camper> campers;
    private Context context;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.camper_row, parent, false);
        Log.d("KGADAPTER", "onCreateViewHolder");
        return new MyViewHolder(itemView);
    }

    public CamperAdapter(Context context, List<Camper> camperList) {
        Log.d("KGADAPTER bjtest", "Instantiating adapter");
        this.context = context;
        this.campers = camperList;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Camper camper = campers.get(position);
        holder.camperFirstName.setText(camper.getFirstName());
        holder.camperLastName.setText(camper.getLastName());

//        holder.expand.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Log.d("KGTEST", "Expanding booking " + booking.getBookingId());
//                // DialogFragment.show() will take care of adding the fragment
//                // in a transaction.  We also want to remove any currently showing
//                // dialog, so make our own transaction and take care of that here.
//                FragmentTransaction ft = ((Activity) context).getFragmentManager().beginTransaction();
//                Fragment prev = ((Activity) context).getFragmentManager().findFragmentByTag("dialog");
//                if (prev != null) {
//                    ft.remove(prev);
//                }
//                ft.addToBackStack(null);
//
//                // Create and show the dialog.
//                DialogFragment newFragment = ExpandedBookingFragment.newInstance(booking);
//                DialogFragment newFragment = ExpandedBookingFragment.newInstance(booking);
//                newFragment.show(ft, "dialog");
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return campers.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView camperFirstName, camperLastName;
        //public ImageButton expand;

        public MyViewHolder(View view) {
            super(view);
            Log.d("KGADAPTER", "Instantiating MyViewHolder");
            camperFirstName = view.findViewById(R.id.textViewCamperFirstName);
            camperLastName = view.findViewById(R.id.textViewCamperLastName);
          //  expand = view.findViewById(R.id.imageButtonExpandBooking);

            //Once we click this row we will want to show more details about this camper.
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("bjtest", "clicked row in list of campers..."+getAdapterPosition());
                    Log.d("bjtest", "Camper details:..."+campers.get(getAdapterPosition()).toString());

                    //Todo  swap to details fragment...
                    //used this resource for the following syntax:  http://code.i-harness.com/en/q/1ba462f
                    AppCompatActivity activity = (AppCompatActivity) v.getContext();
                    activity.getSupportFragmentManager().beginTransaction().replace(R.id.fragment,
                            FragmentHome.newInstance(campers.get(getAdapterPosition()).getFirstName(),"")).addToBackStack(null).commit();
                }
            });
        }
        @Override
        public void onClick(View v) {
            Toast.makeText(v.getContext(), "clicked", Toast.LENGTH_LONG).show();
        }
    }

}
