package info.hccis.cis2250.bjmaclean.canesmacleanbj.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Insert;

import java.util.List;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Business;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Camper;

@Dao
public interface BusinessDao {

    @Query("select * from Business")
    public  List<Business> getBusinesses();

    @Insert
    public void add(Business business);

    @Delete
    public  void delete(Business Business);

    @Query("delete from Business")
    public void deleteAll();


}
