package info.hccis.cis2250.bjmaclean.canesmacleanbj.dao;
import  android.arch.persistence.room.Database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Business;



@Database(entities = {Business.class},version = 1)
public abstract class MyAppDatabase extends RoomDatabase {
    public  abstract BusinessDao businessDao();

}