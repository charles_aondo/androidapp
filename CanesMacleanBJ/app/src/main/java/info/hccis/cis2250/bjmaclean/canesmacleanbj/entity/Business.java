/**
 * Name:Charles Aondo
 * Date;2019-02-14
 * Purpose:This is class for the application
 */
package info.hccis.cis2250.bjmaclean.canesmacleanbj.entity;

import java.io.Serializable;
import java.util.Objects;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;



@Entity(tableName = "Business")
public class Business implements Serializable {
    @PrimaryKey
    private int id;
    private String username;
    private String name;
    private String phoneNumber;
    private String website;
    private String confirmLetterSentDate;
    private String liabilityIdemnificationSentDate;
    private String numberOfPositions;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getConfirmLetterSentDate() {
        return confirmLetterSentDate;
    }

    public void setConfirmLetterSentDate(String confirmLetterSentDate) {
        this.confirmLetterSentDate = confirmLetterSentDate;
    }

    public String getLiabilityIdemnificationSentDate() {
        return liabilityIdemnificationSentDate;
    }

    public void setLiabilityIdemnificationSentDate(String liabilityIdemnificationSentDate) {
        this.liabilityIdemnificationSentDate = liabilityIdemnificationSentDate;
    }

    public String getNumberOfPositions() {
        return numberOfPositions;
    }

    public void setNumberOfPositions(String numberOfPositions) {
        this.numberOfPositions = numberOfPositions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Business business = (Business) o;
        return id == business.id &&
                Objects.equals(username, business.username) &&
                Objects.equals(name, business.name) &&
                Objects.equals(phoneNumber, business.phoneNumber) &&
                Objects.equals(website, business.website) &&
                Objects.equals(confirmLetterSentDate, business.confirmLetterSentDate) &&
                Objects.equals(liabilityIdemnificationSentDate, business.liabilityIdemnificationSentDate) &&
                Objects.equals(numberOfPositions, business.numberOfPositions);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, name, phoneNumber, website, confirmLetterSentDate, liabilityIdemnificationSentDate, numberOfPositions);
    }

    @Override
    public String toString() {
        return "Business{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", website='" + website + '\'' +
                ", confirmLetterSentDate='" + confirmLetterSentDate + '\'' +
                ", liabilityIdemnificationSentDate='" + liabilityIdemnificationSentDate + '\'' +
                ", numberOfPositions='" + numberOfPositions + '\'' +
                '}';
    }
}
