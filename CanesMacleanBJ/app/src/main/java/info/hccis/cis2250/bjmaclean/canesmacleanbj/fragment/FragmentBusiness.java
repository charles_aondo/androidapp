package info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment;


import android.arch.persistence.room.Room;
import android.content.Context;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.R;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.adapter.BusinessAdapter;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Business;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.dao.MyAppDatabase;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Camper;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.util.Utility;


/**

 */
public class FragmentBusiness extends Fragment {
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "businessName";
    private static final String ARG_PARAM2 = "businessUserName";

    private String mParam1;
    private String mParam2;

    private RecyclerView recyclerView;
    private BusinessAdapter businessAdapter;
    private List<Business> businessList = new ArrayList<Business>();

    public static MyAppDatabase myAppDatabase;

    private OnFragmentInteractionListener mListener;

    public FragmentBusiness() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FragmentMacLeanBJ.
     */
    public static FragmentBusiness newInstance(String param1, String param2) {
        FragmentBusiness fragment = new FragmentBusiness();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Bundle tempArgs = getArguments();

        Log.d("bjtest","finished with the background and continuing.");

        recyclerView = getView().findViewById(R.id.recyclerViewBusinesses);
        businessAdapter = new BusinessAdapter(getActivity(), businessList);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(businessAdapter);

        myAppDatabase = Room.databaseBuilder(getContext(),MyAppDatabase.class, "businessDP").allowMainThreadQueries().build();
        //Trigger the background process to get the campers from the service.
        new HttpRequestTask().execute();

    }


    /**
     * This method will connect to the web service and set the list of campers based on what is returned.
     * @since 20190125
     * @author BJM
     */
    private class HttpRequestTask extends AsyncTask<Void, Void, Business[]> {
        @Override
        protected Business[] doInBackground(Void... params) {
            try {
//                SharedPreferences sharedPref = getActivity().getPreferences(Context.MODE_PRIVATE);
//                int userId = sharedPref.getInt("userId", 0);

                final String url = getString(R.string.SERVICE_BUSINESS);
                Log.d("BJTEST", "doInBackground: " + url);
                RestTemplate restTemplate = new RestTemplate();
                restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
                Business[] businesses = null;
                try {
                    businesses = restTemplate.getForObject(url, Business[].class);
                    Log.d("service","get services:"+businesses);
                } catch (Exception e) {
                    Log.d("service","get services:"+e.getMessage());
                }

                return businesses;
            } catch (Exception e) {
                Log.e("MainActivity", e.getMessage(), e);
            }

            return null;
        }
        //Method that shows when there are no campers in the database
        protected void onPostExecute(Business[] businesses) {
//            businessList.clear();
//            businessList.addAll(Arrays.asList(businesses));
//            Log.d("bjtest load campers","Loaded campers from rest, ("+businessList.size()+" campers loaded)");

            if(businesses != null && businesses.length>0){

                businessList.clear();
                businessList.addAll(Arrays.asList(businesses));
//                Log.d("testSize","Getting size from database"+FragmentBusiness.myAppDatabase.businessDao().getBusinesses().size());
                ArrayList<Business> myBusinessList = new ArrayList<Business>(FragmentBusiness.myAppDatabase.businessDao().getBusinesses());
                if(Utility.DEBUGGING) Utility.showDialog(getActivity(), "test", ""+myBusinessList.size());

                //Delete
                FragmentBusiness.myAppDatabase.businessDao().deleteAll();

                //insert all
                for(Business business: businesses){
                    FragmentBusiness.myAppDatabase.businessDao().add(business);
                }
            }
            //If there is no network connection, load from the room database
            else {
                Log.d("roomTest","Loading from room database");
                ArrayList<Business> myBusinessList = new ArrayList<Business>(FragmentBusiness.myAppDatabase.businessDao().getBusinesses());
                if(Utility.DEBUGGING) Utility.showDialog(getActivity(),"test","Loading from "+myBusinessList.size());

                businessList.clear();
                businessList.addAll(myBusinessList);
            }
            Log.d("bjtest load campers","Loaded campers from rest, ("+businessList.size()+" campers loaded)");
            businessAdapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business, container, false);

        return view;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(0);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(int businessID);
    }
}
