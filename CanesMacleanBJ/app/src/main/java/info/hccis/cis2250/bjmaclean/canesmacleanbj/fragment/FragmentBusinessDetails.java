
/**
 * Name:Charles Aondo
 * Date;2019-02-14
 * Purpose:This is
 */
package info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


import info.hccis.cis2250.bjmaclean.canesmacleanbj.R;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.entity.Business;


public class FragmentBusinessDetails extends Fragment{
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "Business";

    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
//Declaring variables
    //private Button buttonLogin;
    private EditText editTextId;
    private EditText editTextBusinessName;
    private EditText editTextBusinessUsername;
    private EditText editTextPhoneNumber;
    private EditText editTextWebsite;
    private EditText editTextPositions;
    private EditText editTextConfirmLetter;
    private EditText editTextLiabilityLetter;
    private EditText sendMessage;


    Intent mailIntent = new Intent(Intent.ACTION_VIEW);
    // Creates a new Intent to insert a contact


    public FragmentBusinessDetails() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters. test
     *
     * @param business Parameter 1.
     * @return A new instance of fragment FragmentMacLeanBJ.
     */
    public static FragmentBusinessDetails newInstance(Business business) {
        FragmentBusinessDetails fragment = new FragmentBusinessDetails();
        Log.d("bjtest business details", business.toString());
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM1, business);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        final Bundle tempArgs = getArguments();
        Business business = (Business) tempArgs.getSerializable(ARG_PARAM1);
        Log.d("bjtest in onStart","camper details="+business.toString());
        //Get references to the buttons.
        //buttonLogin = getView().findViewById(R.id.btn_login);
        editTextId = getView().findViewById(R.id.input_businessid);
        editTextBusinessName = getView().findViewById(R.id.input_business_name);
        editTextBusinessUsername = getView().findViewById(R.id.input_business_username);
        editTextPhoneNumber= getView().findViewById(R.id.input_phone_number);
        editTextWebsite = getView().findViewById(R.id.input_website);
        editTextPositions = getView().findViewById(R.id.input_number_positions);
        editTextConfirmLetter = getView().findViewById(R.id.input_confirm_letter_sent);
        editTextLiabilityLetter = getView().findViewById(R.id.input_liability_letter);



        editTextId.setText(String.valueOf(business.getId()));
        editTextBusinessName.setText(business.getName());
        editTextBusinessUsername.setText(business.getUsername());
        editTextPhoneNumber.setText(business.getPhoneNumber());
        editTextWebsite.setText(business.getWebsite());
        editTextPositions.setText(business.getNumberOfPositions());
        editTextConfirmLetter.setText(business.getConfirmLetterSentDate());
        editTextLiabilityLetter.setText(business.getLiabilityIdemnificationSentDate());


        editTextBusinessName.setTextColor(Color.BLACK);
        editTextId.setTextColor(Color.BLACK);
        editTextBusinessUsername.setTextColor(Color.BLACK);
        editTextPhoneNumber.setTextColor(Color.BLACK);
        editTextWebsite.setTextColor(Color.BLACK);
        editTextLiabilityLetter.setTextColor(Color.BLACK);
        editTextPositions.setTextColor(Color.BLACK);
        editTextConfirmLetter.setTextColor(Color.BLACK);

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }
    //This method sends email to the particular business selected
    protected void sendEmail() {
        final Bundle tempArgs = getArguments();
        Business business = (Business) tempArgs.getSerializable(ARG_PARAM1);
        Log.i("Send email", "");
        String[] TO = {business.getUsername()};
        String[] CC = {};
        String[] subject = {business.getName()};
        sendMessage = getView().findViewById(R.id.input_email_message);
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        emailIntent.putExtra(Intent.EXTRA_TEXT, sendMessage.toString());

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            Log.i("Finished sending email.", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    /**
     *

     * This method adds a business contacts details using the contact provider
     * @return
     */
    public  void addContact(){
        //Adding
        final Bundle tempArgs = getArguments();
        Business business = (Business) tempArgs.getSerializable(ARG_PARAM1);
        Intent intent = new Intent(ContactsContract.Intents.Insert.ACTION);
// Sets the MIME type to match the Contacts Provider
        intent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

        // Inserts an email address
        intent.putExtra(ContactsContract.Intents.Insert.EMAIL,business.getUsername());
        /// Inserts an name
        intent.putExtra(ContactsContract.Intents.Insert.NAME, business.getName()+"");
        // Inserts a phone number
      intent.putExtra(ContactsContract.Intents.Insert.PHONE, business.getPhoneNumber());


        try {
            startActivity(Intent.createChooser(intent, "Adding to Contact..."));
            Log.i("Finished sending email.", "");
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getActivity(), "Fialed to send mail.", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_business_detail, container, false);
//        https://stackoverflow.com/questions/45735830/button-inside-a-fragment
        Button sendButton = (Button) view.findViewById(R.id.button_email);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //make your toast here
                sendEmail();
                Toast toast = Toast.makeText(getActivity(),"Send Email",Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        Button contactButton = (Button) view.findViewById(R.id.contact_add);
       contactButton .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //make your toast here
                addContact();
                Toast toast = Toast.makeText(getActivity(),"Adding Contact.......",Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        return view;
    }
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction("test");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }






    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(String test);
    }
}
