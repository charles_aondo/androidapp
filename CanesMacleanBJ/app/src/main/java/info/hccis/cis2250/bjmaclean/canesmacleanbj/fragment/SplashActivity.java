/**
 * Name:Charles Aondo
 * Date;2019-02-14
 * Purpose:This is the splash activity screen
 */
package info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.util.MainActivity;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(SplashActivity.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        },1);
    }
}
