/**
 * Name:Charles Aondo
 * Date;2019-02-14
 * Purpose:This is
 */
package info.hccis.cis2250.bjmaclean.canesmacleanbj.util;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import info.hccis.cis2250.bjmaclean.canesmacleanbj.R;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.UtilityFile;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.AboutFragment;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentBusiness;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentCampers;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentHome;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentLogin;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.HelpFragment;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.FragmentBusinessDetails;
import info.hccis.cis2250.bjmaclean.canesmacleanbj.fragment.GoogleMaps;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        FragmentLogin.OnFragmentInteractionListener,
        FragmentCampers.OnFragmentInteractionListener,
        AboutFragment.OnFragmentInteractionListener,
        HelpFragment.OnFragmentInteractionListener,
        FragmentBusiness.OnFragmentInteractionListener,
        FragmentBusinessDetails.OnFragmentInteractionListener,
        FragmentHome.OnFragmentInteractionListener {

    FragmentLogin fragmentLogin;
    private static boolean loggedInAttribute = false;

    public static boolean getLoggedIn(){
        return loggedInAttribute;
    }

    public static void setLoggedIn(boolean loggedIn){
        loggedInAttribute = loggedIn;
    }

    FragmentManager fm = this.getFragmentManager();
    android.app.FragmentTransaction ft = fm.beginTransaction();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //Setting the home screen

        if(savedInstanceState == null){
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new FragmentHome());
            ft.addToBackStack(null).commit();
        }


    }

    @Override
    protected void onStart() {
        super.onStart();

        //Check to see if the user has logged in.
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String lastUsername = pref.getString("username", "");
        Fragment firstFragment = null;

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // get menu from navigationView
        Menu menu = navigationView.getMenu();

        if(lastUsername.equals("")){
            Log.d("bjtest", "User has not logged in before");
            setLoggedIn(false);
            // find MenuItem you want to change
            MenuItem menuItemLogout = menu.findItem(R.id.nav_logout);
            menuItemLogout.setVisible(false);
            firstFragment = new FragmentLogin();
            MenuItem menuItemCampers = menu.findItem(R.id.nav_campers);
            menuItemCampers.setVisible(false);

        }else{
            setLoggedIn(true);
            MenuItem menuItemLogin = menu.findItem(R.id.nav_login);
            menuItemLogin.setVisible(false);
            firstFragment = FragmentHome.newInstance(lastUsername, "");
        }

        //Send the user to a login fragment
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.fragment, firstFragment);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Fragment sideFragments = new FragmentHome(); //load the home fragment in case this doesn't work

        //noinspection SimplifiableIfStatement
        switch (id){

            case R.id.action_help:{
                sideFragments =new HelpFragment();

                break;
            }
            case R.id.action_about:{
                sideFragments = new AboutFragment();
                break;
            }
        }
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, sideFragments);
        ft.commit();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_login) {

            //Send the user to a login fragment
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            // Replace the contents of the container with the new fragment
            ft.replace(R.id.fragment, new FragmentLogin());
            ft.commit();


        } else if (id == R.id.nav_campers) {
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            // Replace the contents of the container with the new fragment
            ft.replace(R.id.fragment, new FragmentCampers());
            ft.addToBackStack(null).commit();


        }else  if(id == R.id.business_details){
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new FragmentBusiness());
            ft.addToBackStack(null).commit();
        }
        else if (id == R.id.nav_logout) {

            //Clear the username from the SharedPreferences.
            SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            SharedPreferences.Editor editor = pref.edit();
            editor.putString("username", "");
            editor.commit();

            //Set the logged in flag to false.
            MainActivity.setLoggedIn(false);

            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            // get menu from navigationView
            Menu menu = navigationView.getMenu();
            MenuItem menuItemLogout = menu.findItem(R.id.nav_logout);
            menuItemLogout.setVisible(false);
            MenuItem menuItemLogin = menu.findItem(R.id.nav_login);
            menuItemLogin.setVisible(true);
            MenuItem menuItemCampers = menu.findItem(R.id.nav_campers);
            menuItemCampers.setVisible(false);

            //Send the user to a login fragment
            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            // Replace the contents of the container with the new fragment
            ft.replace(R.id.fragment, new FragmentLogin());
            ft.commit();

        } else if (id == R.id.map) {
            Intent intent = new Intent(getApplicationContext(), GoogleMaps.class);
            startActivity(intent);
        }else if(id == R.id.homeFragment){
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment, new FragmentHome());
            ft.commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String test) {
        Log.d("bjtest", "test from activity="+test);

        //Set the username into the Shared Preferences.

        //If you want to access between activities...
        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("username", test);
        editor.commit();

        MainActivity.setLoggedIn(true);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        // get menu from navigationView
        Menu menu = navigationView.getMenu();
        MenuItem menuItemLogout = menu.findItem(R.id.nav_logout);
        menuItemLogout.setVisible(true);
        MenuItem menuItemLogin = menu.findItem(R.id.nav_login);
        menuItemLogin.setVisible(false);
        MenuItem menuItemCampers = menu.findItem(R.id.nav_campers);
        menuItemCampers.setVisible(true);

        //Save the username login attempt in a file
        UtilityFile.writeFileOnInternalStorage( getApplicationContext(), "testout.txt", test);
        Log.d("bjtest", "wrote to file");
        Log.d("bjtest read", UtilityFile.read_file(getApplicationContext(), "testout.txt"));

        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        // Replace the contents of the container with the new fragment
        ft.replace(R.id.fragment, FragmentHome.newInstance(test, ""));
        ft.commit();

    }

    @Override
    public void onFragmentInteraction() {
        Log.d("BJTEST", "This method is implemented in case FragmentHome ever wants to communicate " +
                "with this activity.");
    }

    @Override
    public void onFragmentInteraction(int businessID) {
        Log.d("BJTEST", "This method is implemented in case FragmentCampers ever wants to communicate " +
                "with this activity.");
    }
}
