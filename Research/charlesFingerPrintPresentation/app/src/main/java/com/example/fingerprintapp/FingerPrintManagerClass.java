package com.example.fingerprintapp;

import android.app.Activity;
import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.CancellationSignal;
import android.support.v4.content.ContextCompat;
import android.widget.ImageView;
import android.widget.TextView;

public class FingerPrintManagerClass extends FingerprintManager.AuthenticationCallback {

    private Context context;
    public FingerPrintManagerClass(Context context){
        this.context = context;
    }
    public  void startAuthentication(FingerprintManager fingerprintManager, FingerprintManager.CryptoObject cryptoObject){
        CancellationSignal cancellationSignal = new CancellationSignal();
        //handler is set to null at this point
        fingerprintManager.authenticate(cryptoObject, cancellationSignal, 0,this,null);
    }
    //Validates for errors in the finger print
    @Override
    public void onAuthenticationError(int errorCode, CharSequence errString) {
        this.update("Authentication Error.  "+errString,false);
    }
    //Authentication failed will report when a wrong finger print is used
    @Override
    public void onAuthenticationFailed() {
        this.update("Authentication failed",false);
    }
    //Ask you to your finger print properly on the scanner, or when you place a finger too fast
    @Override
    public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
        this.update("Error:  "+helpString,false);
    }
    //Report when you have succeeded and your finger print is allowed
    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        this.update("Boooyyaaa!! Ya In",true);
    }
    //Method is created here that updates the UI
    private void update(String s, boolean b) {
        TextView myParalabel = (TextView)((Activity)context).findViewById(R.id.paralabel);
        ImageView imageView  = (ImageView)((Activity)context).findViewById(R.id.fingerPrintImage);

        myParalabel.setText(s);
        if(b== false){
            myParalabel.setTextColor(ContextCompat.getColor(context,R.color.colorAccent));
        }else {
            //Thoughts about implementing this code to work within an app, this else statement will be the place to call the fragment or method?
            myParalabel.setTextColor(ContextCompat.getColor(context,R.color.colorPrimary));
            imageView.setImageResource(R.mipmap.done_image);
        }
    }
}
