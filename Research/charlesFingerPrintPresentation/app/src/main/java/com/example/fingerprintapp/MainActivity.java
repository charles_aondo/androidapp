package com.example.fingerprintapp;

import android.Manifest;
import android.app.KeyguardManager;
import android.content.pm.PackageManager;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import java.security.KeyStore;

import javax.crypto.Cipher;

public class MainActivity extends AppCompatActivity {

    private  final String MY_KEY_NAME = "myKeys";
    private TextView myHeadingTag;
    private ImageView myFingerPrintImage;
    private TextView myParalabel;
    private FingerprintManager fingerprintManager;
    private KeyguardManager keyguardManager;
    private KeyStore keyStore;
    private Cipher cipher;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        myHeadingTag = (TextView)findViewById(R.id.headingTag);
        myFingerPrintImage = (ImageView)findViewById(R.id.fingerPrintImage);
        myParalabel = (TextView)findViewById(R.id.paralabel);
        keyguardManager  = (KeyguardManager) getSystemService(KEYGUARD_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            fingerprintManager = (FingerprintManager) getSystemService(FINGERPRINT_SERVICE);
            if(!fingerprintManager.isHardwareDetected()){

                myParalabel.setText("Finger print Scanner not detected");

            }else if(ContextCompat.checkSelfPermission(this, Manifest.permission.USE_FINGERPRINT)!= PackageManager.PERMISSION_GRANTED){
                myParalabel.setText("Permission not granted");

            }else  if(!keyguardManager.isKeyguardSecure()){
                myParalabel.setText("This device is not secured!");
            }else  if(!fingerprintManager.hasEnrolledFingerprints()){
                myParalabel.setText("Must have one finger print registered");
            }else {
                myParalabel.setText("Place your finger on the scanner");

              //  generateKey();
//                if(cipherInit()) {
//                    FingerprintManager.CryptoObject cryptoObject = new FingerprintManager.CryptoObject(cipher);
//
//                }
                FingerPrintManagerClass fingerPrintManagerClass = new FingerPrintManagerClass(this);
                //Disabling cipher, so crypto object must be set to null
                fingerPrintManagerClass.startAuthentication(fingerprintManager, null);
            }
        }
    }
    //Method that will generate the keystroke
//    public void generateKey(){
//        try{
//            KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
//            KeyGenerator keyGenerator = KeyGenerator.getInstance(KeyProperties.KEY_ALGORITHM_AES, "AndroidKeyStore");
//            keyStore.load(null);
//            keyGenerator.init(new KeyGenParameterSpec.Builder(MY_KEY_NAME,
//                    KeyProperties.PURPOSE_ENCRYPT |                                                        KeyProperties.PURPOSE_DECRYPT)
//                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC) .setUserAuthenticationRequired(true)
//                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
//                    .build());
//            keyGenerator.generateKey();
//        }catch (KeyStoreException | IOException | CertificateEncodingException | NoSuchAlgorithmException
//            | InvalidAlgorithmParameterException | NoSuchProviderException e){
//            e.printStackTrace();
//        } catch (CertificateException e) {
//            e.printStackTrace();
//        }
//    }
    //
//    public boolean cipherInit(){
//        try{
//            cipher = Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES+ "/"+KeyProperties.BLOCK_MODE_CBC+ "/"+KeyProperties.ENCRYPTION_PADDING_PKCS7 );
//        } catch (NoSuchAlgorithmException | NoSuchPaddingException e) {
//            throw  new RuntimeException("Failed to getCipher");
//        }
//        try{
//            keyStore.load(null);
//            SecretKey Key = (SecretKey) keyStore.getKey(MY_KEY_NAME, null);
//            cipher.init(ENCRYPT_MODE,Key);
//            return true;
//        }catch (KeyPermanentlyInvalidatedException e){
//            return false;
//        }catch (KeyStoreException | CertificateException | UnrecoverableKeyException e){
//            throw new RuntimeException("Failed to load cipherInit",e);
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InvalidKeyException e) {
//            e.printStackTrace();
//        }
//        return false;
//    }
}
